<p align="center" width="100%">
    <img src="./img/router_256px.png">
</p>

## File Router

[ License ] |
:---------: |

This project is made available under the `GNU Affero General Public License v3.0` for the sole purpose of demonstrating
my programming and software development capabilities. Please adhere to the permissions, conditions and limitations
which are outlined in the `GNU Affero General Public License v3.0`.

The license file is included as `LICENSE` in the root directory of this project and can also be found online
<a href="https://opensource.org/licenses/AGPL-3.0" title="GNU Affero General Public License v3.0">here</a>.


<br>
<br>

[ System Specs ] |
:---------: |

This was the system configuration of the local development and test environments. Whilst it is recommended to have a
similar setup, newer versions of the toolsets shouldn't break the build.

|Software| Version
|:------------: | :-------------:|
Java Runtime Environment | build 1.8.0_212-b10 x64
Java Development Kit | build 1.8.0_212 x64
Gradle | 5.4.1
Windows 10 Pro | 1903 x64

<br>
<br>

|[ Usage & Build ] |
|:---------: |

[![pipeline status](https://gitlab.com/surahman/file-router/badges/main/pipeline.svg)](https://gitlab.com/surahman/file-router/-/commits/main)

- [X] Executing `gradle build` in the respective __Client/Server__  root directories will build them. The build process
will generate fat jar files in the `/build/libs/` directories for each target.
- [x] Included in the root directory of this project is the archive `File Router-1.1.zip` which has a self-contained
fat jar files. To run the application simply extract the contents of the archive, open a command prompt and navigate to
where you extracted the files.

<br/>

Running the command `java -jar Server-1.1.jar` will launch the _*Server*_ and output the file `port` which contains
the port on which the file router is listening. Please refer to the _Architecture: Server_ section below for further
details.

<br/>


Running the command `java -jar Client-1.1.jar [arguments...]` will launch the _*Client*_. Please refer to the
_Architecture: Client_ section below for further details on parameters.

<br/>

The _*Server*_ must be launched first. A connection from a _*Client*_ requesting a file is required before a _*Client*_
trying to upload the file with a specified `key` connects. The downloading _*Client*_ can sit and wait for an unbound
amount of time waiting for an uploading _*Client*_.

_Example Run:_
```bash
java -jar Server-1.1.jar

java -jar Client-1.1.jar localhost 1723 Dlas55564 uploadTest 100

java -jar Client-1.1.jar localhost 1723 Ulas55564 testData136kb 50 5

java -jar Client-1.1.jar localhost 1723 Q
```

_SHA256 Hash of files:_
```bash
Get-FileHash .\testData136kb

Algorithm       Hash                                                                   Path
---------       ----                                                                   ----
SHA256          9AED7697B4A5DAEDB2D239DB831C84D705B920DC670CFA283418B52999EC83ED       ...\testD...


Get-FileHash .\uploadTest

Algorithm       Hash                                                                   Path
---------       ----                                                                   ----
SHA256          9AED7697B4A5DAEDB2D239DB831C84D705B920DC670CFA283418B52999EC83ED       ...\uploa...
```

<br>
<br>

|[ Architecture ] |
|:---------:|
##### Design ![Design](./img/router_24px.png)

The purpose of this project is to provide asynchronous file exchange through an intermediary routing _*Server*_. Files
transferred between _*Client*_s are not cached at the routing _*Server*_ but father forwarded immediately to the
requesting _*Client*_s.

A requesting _*Client*_ connects to the routing *_Server_* and provides it with a file _*key*_. When a serving _*Client*_
connects to the routing *_Server_* the request is matched and the transfer is facilitated. Once the file transfer is
completed both the sending and receiving clients exit without intervention.

<br/>

###### Protcol ![Protocol](./img/protocol_16px.png)
Data transfered between the _*Client*_ and _*Server*_ adhere to the following format.
* **Command**: 1 ASCII character: D (download), U (upload), or Q (terminate *_Server_*).
* **Key**: 9 ASCII characters. The key is padded with `\0`-characters to fill the key buffer, but all strings are null
terminated leaving only 8 ASCII characters for the key.

When initiating an upload, the above 10-byte control information is immediately followed by the binary data stream of the
file. When initiating a download, the _*Server*_ responds with the binary data stream of the file. Once a _*Client*_ has
completed a file upload, it closes it's connection. The _*Server*_ then closes the download connection to the downloading
 _*Client*_.

<br/>

###### Server ![Server](./img/router_16px.png)
The implementation of the _*Server*_ is multithreaded and creates a TCP/IP tunnel for each file request. Each thread is
stored in a `HashMap` keyed on its `key`, this allows for very fast constant time lookups. Once a pair is matched the
downloader thread is awoken by signaling its condition variable (non-busy waiting) and the file transfer is begun.

Mutual exclusion of the `HashMap` is unnecessary as only the main thread that is spurned is active in the critical
section. The `tunnel` is using condition variables as a means of signalling; no mutual exclusion is required here
as execution within the threads critical region is sequential. Any inbound file chunk transfer is met with an
outbound file transfer in a sequential loop. The only shared resource between the inbound/outbound sockets is the
file chunk buffer.

<br/>

###### Client ![Client](./img/upload_16px.png) ![Client](./img/download_16px.png)
The implementations of both the _*Uploader*_ and _*Downloader*_ is in the same file, and is handled by two different
constructors which handle the hand off based on the number of parameters being passed to them (polymorphism). The
terminate signal is sent via a _*Client*_ using the `Q` key and causes the _*Server*_ to terminate all the waiting
download _*Client*_s. This allows for active transfers to continue to completion (connection draining).

A file for upload is completely read in to memory and broken down into `send size` chunks for transfer. File chunks for
download are stored in memory of `recv size` chunks until a transfer is completed, at which point the file is written
to disk. This behavior minimizes the delay between sends/receives as disk read/writes are time expensive operations.

<br>
<br>

|[ Features ] |
|:---------:|

##### Parameters:

###### _*Server*_ ![Client](./img/router_24px.png)
- [x] The _*Server*_ does not take parameters.

###### _*Client*_ ![Client](./img/upload_24px.png) ![Client](./img/download_24px.png)
- [x] Terminate _*Server*_:
```bash
client <host> <port> Q
```
The _*Client*_ sends an empty key buffer filled with null characters to the _*Server*_ on termination request.

- [x] Upload :
```bash
client <host> <port> D<key> <file name> <recv size>
```

- [x] Download :
```bash
client <host> <port> U<key> <file name> <send size> <wait time>
```
* **host**: IP or Canonical name address of the _*Server*_.
* **port**: Port number that the _*Server*_ is listening on.
* **key**: A null terminated key buffer consisting of 8-chars.
* **file name**: Name of file to send or write to disk.
* **recv/send size**: Size of buffer to transmit during upload/download.
* **wait time**: Time delay _(ms)_ between each subsequent buffer read/write.

<br/>

##### Server ![Server](./img/router_24px.png)
- [x] Connections: Allows for an arbitrary number of concurrent connections and file transfers.
- [x] Socket: Creates and opens a `TCP/IP` socket on automatically OS-assigned port number.
- [x] Port: Prints out the listening port number to `stdout` as well as to a file named `port`.
- [x] Non-blocking: Accepts all _*Client*_ connections as they arrive without blocking.
- [x] Commands: Handles connections from _*Client*_s and commands to upload/download or terminate the _*Server*_.
- [x] Caching: No files are cached on the _*Server*_.
- [x] Key Indexing: Is always able to match an upload/download _*Clients*_ based on the file `key` they provide.
- [x] Assumptions: Only a single downloader matches and uploader. Downloader always connects before an uploader.
- [x] STDOUT: Messages displayed under startup/shutdown:
_Startup:_
```java
[ TCP/IP TUNNEL SERVER STARTED: <host>:<port> ]
```

_Shutdown:_
```java
[ Beginning shutdown sequence. ]
[ Shutting down: <key> ] // Once for each active file transfer of <key>.
[ Shutdown sequence complete. ]
```

<br/>

##### Client ![Client](./img/upload_24px.png)![Client](./img/download_24px.png)
- [x] Connections: Connects to a _*Server*_ at the specified `host` address listening on the specified `port` on
a `TCP/IP` connection.
- [x] Terminate _*Server*_: Transmits the `Q` ASCII along with a `key` buffer filled with null characters to the
_*Server*_.
- [x] File Name: Reads data from or stores data to the file name specified in `file name`.
- [x] File Size: Sends or receives file chunks of a buffer size specified in `recv/send size`. The final chunk of the
file may be smaller than the size specified to allow transmission completion.
- [x] Delay: Allows for a delay to be introduced between file buffer chunk transmission _(ms)_. This allows for
concurrency testing of the _*Server*_.


<br/>
<br/>


---

###### Copyright and Legal Notice(s):
&copy; Saad Ur Rahman. All rights reserved. Usage and source code is licensed under the
`GNU Affero General Public License v3.0`.

<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>.</div>

---
