            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    TCPTunnel.java                  #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                TCP/IP Tunnel Server               #
            #__________________________________________________*/




//- Import(s): Library:
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class TCPTunnel extends Thread {

    /* TCPTunnel:
    *   Constructor to setup TCP/IP Tunnel.
    * -Parameters:  Socket sock
    * -Returns:     n/a
    * -Throws:      Exception
    */
    TCPTunnel(Socket sock, String key) throws Exception
    {

        // Setup Data Member(s):
        m_Key = key;
        m_TCPDownloaderSock = sock;
        m_ReLock = new ReentrantLock();
        m_CondLock = m_ReLock.newCondition();

        // Set threads ID.
        this.setName(m_Key);
    }



    /* run:
    *   Main driver routine for the thread.
    * -Parameters:  n/a
    * -Returns:     n/a
    * -Throws:      n/a
    */
    public void run()
    {
        // Display setup message.
        System.out.println("SRV DWN WAIT KEY: " + m_Key);

        // Acquire lock and await signal from uploader.
        try
        {
            m_ReLock.lock();
            m_CondLock.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        // RETURN: Terminate thread if uploader socket was not set.
        if (m_TCPUploaderSock == null)
        {
            System.out.println("SRV DWN QUIT KEY: " + m_Key);
            m_ReLock.unlock();
            return;
        }


        // RETURN: Display transfer beginning message. Xfer using the <tunnel>.
        System.out.println("SRV UPL BEGIN KEY: " + m_Key);
        try
        {
            tunnel();
        }
        catch (Exception e)
        {
            System.out.println("Error executing TCP tunnel: " + m_Key + ": " + e.getMessage());
            e.printStackTrace();
        }
        System.out.println("SRV XFR COMPLETE KEY: " + m_Key);

        // Shutdown TCP sockets.
        try
        {
            m_TCPDownloaderSock.close();
            m_TCPUploaderSock.close();
        }
        catch (IOException e)
        {
            System.out.println("Error closing TCP sockets in key: " + m_Key + ": " + e.getMessage());
        }

    }



    /* tunnel:
    *   Pass data segment from uploader to downloader - provide tunnel service.
    * -Parameters:  n/a
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void tunnel() throws Exception
    {

        // Variable Declaration(s):
        DataInputStream inputStream;            // Input stream to read from socket.
        DataOutputStream outStream;             // Output stream to write to socket.
        byte[] buffer;                          // Inbound chunk of data.
        int recvSize;                           // Bytes received on inbound xfer.

        // Setup byte buffer for inbound files.
        try
        {
            buffer = new byte[m_TCPUploaderSock.getReceiveBufferSize()];
        }
        catch (SocketException e)
        {
            throw new SocketException("Error setting up buffer using uploader socket size: " + e.getMessage());
        }

        // Setup the input/output streams.
        try
        {
            inputStream = new DataInputStream(m_TCPUploaderSock.getInputStream());
            outStream = new DataOutputStream(m_TCPDownloaderSock.getOutputStream());
        }
        catch (IOException e)
        {
            throw new IOException("Unable to setup read/write from/to TCP socket: " + e.getMessage());
        }

        // Read from uploader socket till it's closed and pass off to the downloader socket
        try
        {
            // Indicate send status on server.
            System.out.println("XFR SND DAT KEY: " + m_Key);

            int counter = 0;
            while((recvSize = inputStream.read(buffer)) != -1)
            {
                outStream.write(buffer,0, recvSize);
                outStream.flush();
                buffer = new byte[recvSize];
                ++counter;
                //System.out.println("XFR SND DAT SEG: " + counter + " Key: " + m_Key + " Size: " + recvSize);
            }

            // Indicate segments transferred.
            System.out.println("XFR SND DAT END KEY: " + m_Key + ", SEG [0 - " + counter + " ]");

        }
        catch (Exception e)
        {
            throw new Exception("Error reading from TCP socket and writing to buffer.");
        }
        finally
        {
            // Close the input stream and cause flush.
            inputStream.close();
            outStream.close();
        }

    }



    //-- Data Member(s):
    private final String m_Key;                 // File access key.
    private final Socket m_TCPDownloaderSock;   // Socket for outbound/downloader TCP/IP connections.
    public Socket m_TCPUploaderSock;            // Socket for inbound/uploader TCP/IP connections.
    public Lock m_ReLock;                       // Reentrant lock for this thread.
    public Condition m_CondLock;                // Condition lock for this thread.
}
