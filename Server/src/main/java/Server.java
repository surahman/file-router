            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Server.java                     #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               TCP/IP Tunnel Server                #
            #__________________________________________________*/




//- Import(s): Library:
import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Server {

    /* Server:
    *   Constructor to setup server. Constructor bootstraps server.
    * -Parameters:  n/a
    * -Returns:     n/a
    * -Throws:      Exception
    */
    Server() throws Exception
    {

        // Setup data member(s):
        m_Active = true;
        m_SocketMap = new HashMap();

        // Bootstrap listening socket.
        Bootstrap();


        // Main server loop.
        while (m_Active)
        {

            // Capture inbound TCP/IP connection.
            try
            {
                Socket clientSock = m_TCPServSock.accept();

                // Get file key information.
                getKey(clientSock);

                // Operation: [ U | D | Q ].
                switch (m_Oper)
                {
                    // Downloader call.
                    case 'D':
                        SetupFileRequest(clientSock);
                        break;
                    // Uploader call.
                    case 'U':
                        SetupFileDelivery(clientSock);
                        break;
                    // Termination call.
                    case 'Q':
                        shutdown();
                        break;
                    // Unknown.
                    default:
                        System.out.println("Received unknown operation call.");
                        break;
                }

            }
            catch (IOException e)
            {
                throw new IOException("Error accepting and setting up inbound client socket.");
            }
        }


        // Shutdown server socket.
        m_TCPServSock.close();

    }



    /* Bootstrap:
    *   Setup the TCP/IP socket and write <port> file to disk.
    * -Parameters:  n/a
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void Bootstrap() throws Exception
    {

        // Variable Declaration(s):
        int serverPort;                         // Servers listening port number.
        String serverAddr;                      // Servers listening address.


        // Setup TCP/IP server listening socket.
        try
        {
            m_TCPServSock = new ServerSocket(0);
        }
        catch (IOException e)
        {
            throw new IOException("Error setting up TCP/IP server listening socket.");
        }


        // Write <port> file to disk.
        try
        {
            serverPort = m_TCPServSock.getLocalPort();
            serverAddr = InetAddress.getLocalHost().getHostName();

            PrintWriter fout = new PrintWriter("port");
            fout.println(serverPort);
            fout.close();
        }
        catch (FileNotFoundException e)
        {
            throw new FileNotFoundException("Error writing <port> file to disk.");
        }

        // Display server information.
        System.out.println("[ TCP/IP TUNNEL SERVER STARTED: " + serverAddr + ":" + serverPort + " ]");

    }



    /* getKey:
    *   Extract the file key and operation: [ U | D | Q ].
    * -Parameters:  Socket sock
    * -Returns:     n/a
    * -Throws:      IOException
    */
    private void getKey(Socket sock) throws IOException
    {

        // Variable Declaration(s):
        BufferedReader inputStream;             // Input stream to read from socket.

        // Setup the input stream.
        try
        {
            inputStream = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        }
        catch (IOException e)
        {
            throw new IOException("Unable to setup read from TCP socket.");
        }

        // Read key from socket.
        try
        {
            // Key conversion helpers for input.
            int input;
            StringBuilder rawString = new StringBuilder();

            for (int i = 0; i < 9; ++i)
            {
                // Get single character from input.
                input = inputStream.read();
                char ch = (char) input;

                // Build full string of input.
                rawString.append(ch) ;
            }
            m_Key = rawString.toString();

            // Separate Key to operator and actual key.
            m_Oper = m_Key.charAt(0);
            m_Key = m_Key.substring(1);
        }
        catch (IOException e)
        {
            throw new IOException("Unable to read key from TCP socket.");
        }

    }



    /* SetupFileRequest:
    *   Setup downloader thread: [ D ].
    * -Parameters:  Socket sock
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void SetupFileRequest(Socket sock)
    {
        try
        {
            // setup and start the <TCPTunnel> thread.
            TCPTunnel tunnel = new TCPTunnel(sock, m_Key);
            tunnel.start();

            // Create <TCPTunnel> corresponding to file.
            m_SocketMap.put(m_Key, tunnel);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    /* SetupFileDelivery:
    *   Setup the inbound-outbound connection relay.
    * -Parameters:  Socket sock
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void SetupFileDelivery(Socket sock)
    {
        // Lookup file key and get <tunnel> thread.
        TCPTunnel tunnel = m_SocketMap.get(m_Key);

        // Check to make sure there is a client requesting the specified key.
        try
        {
            if (tunnel == null)
            {
                System.out.println("Error, no client requesting file with key: " + m_Key);
                sock.close();
                return;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // Update the thread with the inbound uploader sock and awaken it.
        tunnel.m_ReLock.lock();
        tunnel.m_TCPUploaderSock = sock;
        tunnel.m_CondLock.signal();
        tunnel.m_ReLock.unlock();
    }



    /* shutdown:
    *   Shutdown the TCP/IP Tunnel Server, close all inactive connections.
    * -Parameters:  n/a
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void shutdown() throws Exception
    {
        // Display shutting down message.
        System.out.println("[ Beginning shutdown sequence. ]");

        // Close the server listening socket.
        try
        {
            m_TCPServSock.close();
            m_Active = false;
        }
        catch (IOException e)
        {
            throw new IOException("Error shutting down server listening socket: " + e.getMessage());
        }

        // Loop over all tunnel threads.
        for (HashMap.Entry<String, TCPTunnel> entry : m_SocketMap.entrySet())
        {
            // Get tunnel thread.
            TCPTunnel tunnel = entry.getValue();

            // Check if its inactive and restart.
            if (tunnel == null)
            {
                continue;
            }

            // Signal sleeping thread to exit.
            if (tunnel.m_TCPUploaderSock == null)
            {
                tunnel.m_ReLock.lock();
                tunnel.m_CondLock.signal();
                tunnel.m_ReLock.unlock();
            }

            // Wait for thread to exit, and then allow loop to restart.
            try
            {
                System.out.println("[ Shutting down: " + tunnel.getName() + " ]");
                tunnel.join();
            }
            catch (InterruptedException e)
            {
                throw new InterruptedException("Error shutting down tunnel thread " + tunnel.getName() +
                                                ": " + e.getMessage());
            }
        }

        // Indicate shutdown.
        System.out.println("[ Shutdown sequence complete. ]");
    }



    //-- Data Member(s):
    private ServerSocket m_TCPServSock;             // Socket for inbound TCP/IP connections.
    private HashMap<String, TCPTunnel> m_SocketMap; // Hash map of all sockets/connections.
    private boolean m_Active;                       // Server status.
    private String m_Key;                           // File key.
    private char m_Oper;                            // Operation: [ U | D | Q ].
}
