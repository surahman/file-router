            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    ServerMain.java                 #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                TCP/IP Tunnel Server               #
            #__________________________________________________*/




            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class ServerMain {

    public static void main(String[] args)
    {

        // Start up server.
        try
        {
            Server server = new Server();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }

    }
}
