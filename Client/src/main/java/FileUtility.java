            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    FileUtility.java                #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Client [Uploader/Downloader]           #
            #__________________________________________________*/



//- Import(s): Library:
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class FileUtility {

    /* FileUtility:
    *   Constructor to setup data members and set mode.
    * -Parameters:  char option, String fileName, int chunkSize
    * -Returns:     n/a
    * -Throws:      Exception
    */
    FileUtility(char mode, String fileName, int chunkSize) throws Exception
    {
        // Setup data members:
        m_fileName = fileName;
        m_ChunkSize = chunkSize;


        // Setup the appropriate file reader/writer.
        switch (mode)
        {
            // Setup file writer for download.
            case 'D':

                // Setup the binary chunk list.
                m_InboundList = new LinkedList<byte[]>();
                break;

            // Send file reader for upload.
            case 'U':
                readBinary(fileName);
                break;

            // Illegal option.
            default:
                throw new IllegalArgumentException("<FileUtility> only operates in [D | U] mode.");
        }
    }



    /* readBinary:
    *   Read in binary data of file for upload.
    * -Parameters:  String fileName
    * -Returns:     n/a
    * -Throws:      IOException
    */
    private void readBinary(String fileName) throws IOException
    {
        // Read raw data file in.
        byte[] rawFile;
        try
        {
            rawFile = Files.readAllBytes(Paths.get(fileName));
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new IOException("Error reading in file " + fileName);
        }

        // Setup size of <m_PacketQueue>, add one position for <EOT> packet.
        int queueSize = ((int) Math.ceil((double) rawFile.length / m_ChunkSize));
        m_OutboundQueue = new byte[queueSize][];

        // Build the array of packets. 500byte payloads.
        for (int i = 0, packetNum = 0; i < rawFile.length; i += m_ChunkSize, ++packetNum)
        {

            // <m_ChunkSize> Bytes payload.
            int upperBound = Math.min(i + m_ChunkSize, rawFile.length);
            m_OutboundQueue[packetNum] = Arrays.copyOfRange(rawFile, i, upperBound);
        }
    }



    /* addDataSegment:
    *   Add a data segment to inbound packet buffer.
    * -Parameters:  byte[] segment, int recvSize
    * -Returns:     n/a
    * -Throws:      Exception
    */
    public void addDataSegment(byte[] segment, int recvSize) throws Exception
    {
        // Resize the byte buffer if required.
        if (recvSize < segment.length)
        {
            byte[] buffer = Arrays.copyOfRange(segment, 0, recvSize);
            m_InboundList.add(buffer);
        }
        // Otherwise just add the original segment.
        else
        {
            m_InboundList.add(segment);
        }
    }



    /* WriteToFile:
    *   Write complete file in buffer to file.
    * -Parameters:  byte[] segment
    * -Returns:     n/a
    * -Throws:      Exception
    */
    public void WriteToFile() throws Exception
    {
        // Variable Declaration(s):
        FileOutputStream fout;                  // File handle to write through.

        // Setup file output stream.
        try
        {
            fout = new FileOutputStream(m_fileName, true);
        }
        catch (FileNotFoundException e)
        {
            throw new FileNotFoundException("Unable to create and write to " + m_fileName);
        }

        // Write byte segment-by-segment to file, starting at bottom.
        for (byte[] segment : m_InboundList)
        {
            fout.write(segment);
        }

        // Close file handle to flush buffer.
        fout.close();
    }



    //-- Data Member(s):
    final private String m_fileName;            // Name of file to written.
    final private int m_ChunkSize;              // Data chunk size.
    private LinkedList<byte[]> m_InboundList;   // Inbound transmission list.
    byte[][] m_OutboundQueue;                   // Outbound transmission queue.
}
