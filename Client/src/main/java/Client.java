            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Client.java                     #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Client [Uploader/Downloader]           #
            #__________________________________________________*/



//- Import(s): Library:
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import static java.lang.Thread.sleep;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Client {

    /* Client:
    *   [ DOWNLOAD | 'D' ]: Constructor.
    * -Parameters:  String hostAddr, int hostPort, String key, String fileName, int size
    * -Returns:     n/a
    * -Throws:      Exception
    */
    Client(String hostAddr, int hostPort, String key, String fileName, int size) throws Exception
    {

        // Data Member(s):
        m_Key = key + new String(new char[9 - key.length()]);
        m_Size = size;                          // Receive buffer size.
        m_SleepDuration = -1;                   // Sleepy time.

        // Setup the download buffer.
        m_FileUtil = new FileUtility('D', fileName, size);

        // Bootstrap the TCP connection to the host.
        Bootstrap(hostAddr, hostPort);

        // Call the downloader.
        downloader();

        // Close TCP socket.
        m_TCPSock.close();
    }



    /* Client:
    *   [ UPLOAD | 'U' ]: Constructor.
    * -Parameters:  String hostAddr, int hostPort, String key, String fileName, int size, int time
    * -Returns:     n/a
    * -Throws:      Exception
    */
    Client(String hostAddr, int hostPort, String key, String fileName, int size, int time) throws Exception
    {

        // Data Member(s):
        m_Key = key + new String(new char[9 - key.length()]);
        m_Size = size;                            // Send buffer size.
        m_SleepDuration = time;                   // Sleepy time.

        // Setup the upload buffer.
        m_FileUtil = new FileUtility('U', fileName, size);

        // Bootstrap the TCP connection to the host.
        Bootstrap(hostAddr, hostPort);

        // Call the uploader.
        uploader();

        // Close TCP socket.
        m_TCPSock.close();
    }



    /* Client:
    *   [ FINISH | 'Q' ]: Constructor.
    * -Parameters:  String hostAddr, int hostPort
    * -Returns:     n/a
    * -Throws:      Exception
    */
    Client(String hostAddr, int hostPort) throws Exception
    {

        // Data Member(s):
        m_Key = "Q" + new String(new char[8]);  // Terminal string.
        m_Size = 9;                             // Send buffer of 9 bytes for signal.
        m_SleepDuration = -1;                   // Sleepy time.

        // Bootstrap the TCP connection to the host.
        Bootstrap(hostAddr, hostPort);

        // Close TCP socket.
        m_TCPSock.close();

        System.out.println("[ SHUTDOWN SIGNAL SENT TO TCP/IP TUNNEL. ]");
    }



    /* Bootstrap:
    *   Setup the TCP/IP socket.
    * -Parameters:  String hostname, int port
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void Bootstrap(String hostname, int port) throws Exception
    {
        // Variable Declaration(s):
        PrintStream outBuffer;                  // Outbound buffer to host.
        int serverPort;                         // Servers listening port number.
        String serverAddr;                      // Servers listening address.

        // Setup TCP socket descriptor to host.
        try
        {
            m_TCPSock = new Socket(hostname, port);
        }
        catch (IOException e)
        {
            throw new IOException("Error setting up TCP socket.");
        }

        // Setup outbound data buffer and TX over <TCP/IP> socket.
        try
        {
            outBuffer = new PrintStream(m_TCPSock.getOutputStream());
        }
        catch (IOException e)
        {
            throw new IOException("Error setting up PrintStream.");
        }

        // Send the key.
        outBuffer.print(m_Key);

        // Force final flush on output buffer.
        outBuffer.flush();

        // Truncate the <m_Key> to just the file key.
        m_Key = m_Key.substring(1);

        // Extract Client address information.
        try
        {
            serverPort = m_TCPSock.getLocalPort();
            serverAddr = InetAddress.getLocalHost().getHostName();
            System.out.println("[ TCP/IP CLIENT STARTED: " + serverAddr + ":" + serverPort + " ]");
        }
        catch (UnknownHostException e)
        {
            throw new UnknownHostException("Error resolving host information: " + e.getMessage());
        }
    }



    /* downloader:
    *   Download data over TCP connection till it's closed.
    * -Parameters:  n/a
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void downloader() throws Exception
    {
        // Variable Declaration(s):
        InputStream inputStream;                // Input stream to read from socket.
        byte[] inbound = new byte[m_Size];      // Inbound chunk of data.
        int counter = 0;                        // Counter for data segments received.
        int recvSize = 0;                       // Bytes received on inbound xfer.

        // Setup the input stream.
        try
        {
            inputStream = m_TCPSock.getInputStream();
        }
        catch (IOException e)
        {
            throw new IOException("Unable to setup read from TCP socket: " + e.getMessage());
        }

        // Read from TCP socket till it's closed.
        try
        {
            System.out.println("DWN RCV LSN KEY: " + m_Key);

            // Send data segments.
            while((recvSize = inputStream.read(inbound)) != -1)
            {
                // Indicate file xfer has begun.
                if (counter == 0)
                {
                    System.out.println("DWN RCV DAT START KEY: " + m_Key);
                }

                // Add file to file buffer.
                m_FileUtil.addDataSegment(inbound, recvSize);
                ++counter;
                inbound = new byte[recvSize];
            }

            // Indicate segments transferred.
            System.out.println("XFR SND DAT END KEY: " + m_Key + ", SEG [0 - " + counter + " ]");

        }
        catch (Exception e)
        {
            throw new Exception("Error reading from TCP socket and writing to buffer.");
        }
        finally
        {
            // Close the input stream and cause flush.
            inputStream.close();
        }

        // Check if transmission was terminated.
        if (counter == 0)
        {
            System.out.println("[ TRANSMISSION TERMINATED: " + m_Key + " ]");
            return;
        }

        // Write the buffer to disk completing the file.
        m_FileUtil.WriteToFile();
        System.out.println("DWN WRT FILE KEY: " + m_Key);
        System.out.println("[ TRANSMISSION COMPLETED: " + m_Key + " ]");
    }



    /* uploader:
    *   Upload data over TCP connection.
    * -Parameters:  n/a
    * -Returns:     n/a
    * -Throws:      Exception
    */
    private void uploader() throws Exception
    {
        // Variable Declaration(s):
        DataOutputStream outStream;             // Output stream to write to socket.
        int i = 0;                              // Send loop counter.

        // Setup output stream.
        try
        {
            outStream = new DataOutputStream(m_TCPSock.getOutputStream());
        }
        catch (IOException e)
        {
            throw new IOException("Unable to setup write to TCP socket.");
        }

        // Send data from TCP socket till file buffer is expended.
        try
        {
            // Indicate xfer has begun.
            System.out.println("UPL SND DAT START KEY: " + m_Key);

            for (; i < m_FileUtil.m_OutboundQueue.length; ++i)
            {
                outStream.write(m_FileUtil.m_OutboundQueue[i], 0, m_FileUtil.m_OutboundQueue[i].length);
                //System.out.println("UPL SND DAT SEG: " + i);
                sleep(m_SleepDuration);
            }

            // Indicate segments transferred.
            System.out.println("XFR SND DAT END KEY: " + m_Key + ", SEG [0 - " + i + " ]");

        }
        catch (IOException e)
        {
            throw new IOException("Error sending file segment over TCP socket: " + e.getMessage());
        }
        catch (InterruptedException e)
        {
            throw new InterruptedException("Error sleeping thread between sends: " + e.getMessage());
        }
        finally
        {
            // Shutdown the output stream and cause final flush.
            try
            {
                outStream.close();
            }
            catch (IOException e)
            {
                throw new IOException("Error closing output stream.");
            }

            System.out.println("[ TRANSMISSION COMPLETE: " + m_Key + " ]");
        }

    }



    //-- Data Member(s):
    private Socket m_TCPSock;                   // TCP socket descriptor.
    private String m_Key;                       // File key.
    private int m_Size;                         // Send/Receive buffer size.
    private FileUtility m_FileUtil;             // File read/write utility.
    private final int m_SleepDuration;          // Uploader sleep duration between data segments.
}
