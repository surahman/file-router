            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    ClientMain.java                 #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Client [Uploader/Downloader]           #
            #__________________________________________________*/



//- Import(s): Library:
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class ClientMain {

    public static void main(String[] args)
    {
        // Usage:
        // Terminate: client <host> <port> Q<buffer of null keys>
        // Receive:   client <host> <port> D<key> <file name> <recv size>
        // Transmit:  client <host> <port> U<key> <file name> <send size> <wait time>

        try
        {
            // Variable Declaration(s):
            String hostAddr;                    // Host address.
            int hostPort;                       // Host port number.
            String rawKey;                      // Complete key.
            String fileName;                    // Filename inbound/outbound.
            int chunkSize;                      // File chunk size.
            int waitTime = -1;                  // Millisecond delay for send.

            // Check argument count.
            if (args.length < 3 || args.length > 6)
            {
                throw new IllegalArgumentException("Usage: Illegal argument count, please see documentation.");
            }

            // Capture arguments from command line.
            hostAddr = args[0];
            hostPort = Integer.parseInt(args[1]);
            rawKey = args[2];
            //hostPort = readPort();

            // Validate arguments.
            if (hostPort < 0 || hostPort > 65535)
            {
                throw new IllegalArgumentException("Usage: <Port> number outside range.");
            }
            if (rawKey.length() > 9)
            {
                throw new IllegalArgumentException("Usage: <Key> parameter is too large.");
            }

            switch (rawKey.charAt(0))
            {
                // Shutdown inactive connections on server:
                case 'Q':
                    // Validate arguments:
                    if (args.length > 3)
                    {
                        throw new IllegalArgumentException("Usage: Too many parameters for <Q>.");
                    }

                    // Hand off to <Client>.
                    Client terminator = new Client(hostAddr, hostPort);

                    break;

                // Download file from TCP Tunnel:
                case 'D':
                    // Get remainder of arguments:
                    fileName = args[3];
                    chunkSize = Integer.parseInt(args[4]);

                    // Validate arguments:
                    if (args.length != 5)
                    {
                        throw new IllegalArgumentException("Usage: Illegal parameter count for <D>.");
                    }
                    if (chunkSize < 0)
                    {
                        throw new IllegalArgumentException("Usage: Illegal file size.");
                    }

                    // Hand off to <Client>.
                    Client uploader = new Client(hostAddr, hostPort, rawKey, fileName, chunkSize);

                    break;

                // Upload file to TCP Tunnel:
                case 'U':
                    // Get remainder of arguments:
                    fileName = args[3];
                    chunkSize = Integer.parseInt(args[4]);
                    waitTime = Integer.parseInt(args[5]);

                    // Validate arguments:
                    if (args.length != 6)
                    {
                        throw new IllegalArgumentException("Usage: Illegal parameter count for <U>.");
                    }
                    if (chunkSize < 0 || waitTime < 0)
                    {
                        throw new IllegalArgumentException("Usage: Illegal file size or wait time.");
                    }

                    // Hand off to <Sender>.
                    Client downloader = new Client(hostAddr, hostPort, rawKey, fileName, chunkSize, waitTime);

                    break;
                // Illegal option passed:
                default:
                    throw new IllegalArgumentException("Usage: Options: [Q] Shutdown, " +
                                                       "[D] Download file, [U] Upload file.");
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }
    }



    /* readPort:
    *   Main driver routine for the thread.
    * -Parameters:  n/a
    * -Returns:     int
    * -Throws:      Exception
    */
    static int readPort()
    {
        Scanner input = null;
        try
        {
            //input = new Scanner(new File("../CS456_A3_Server/port"));
            input = new Scanner(new File("port"));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return Integer.parseInt(input.next());
    }
}